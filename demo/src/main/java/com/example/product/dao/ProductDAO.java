package com.example.product.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.common.CommonUtil;
import com.example.common.DataTableResults;
import com.example.common.UttData;
import com.example.product.bean.ProductBean;
import com.example.product.bo.ProductBO;
import com.example.product.form.ProductForm;
import com.example.user.entity.UserBean;

@Transactional
@Repository
public interface ProductDAO extends CrudRepository<ProductBO, Long> {
	public List<ProductBO> findAll();

	public default DataTableResults<ProductBean> getDatatables(UttData uttData, ProductForm productForm, HttpServletRequest req) {
        List<Object> paramList = new ArrayList<>();
        String nativeSQL = "SELECT "
                + "     p.product_id AS productId " 
                + "     , p.brand_id as brandId " 
                + "     , p.name as name "
                + "     , p.price as price "
                + "     , p.description AS description "
                + "     , p.category_id AS categoryId "
                + "     , p.created_date AS createdDate "
                + "     , p.created_by AS createdBy "
                + "     , p.updated_date AS updatedDate "
                + "     , p.updated_by AS updatedBy "
                + " FROM product p ";

        StringBuilder strCondition = new StringBuilder(" WHERE 1 = 1 ");
        CommonUtil.filter(productForm.getName(), strCondition, paramList, "p.name");
        String orderBy = " ORDER BY product_id DESC ";
        return uttData.findPaginationQuery(nativeSQL + strCondition.toString(), orderBy, paramList, UserBean.class);
    }
}
