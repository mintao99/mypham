package com.example.product.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.common.CommonUtil;
import com.example.common.Constants;
import com.example.common.DataTableResults;
import com.example.common.Response;
import com.example.controller.BaseController;
import com.example.exception.SysException;
import com.example.product.bean.ProductBean;
import com.example.product.bo.ProductBO;
import com.example.product.form.ProductForm;
import com.example.product.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController extends BaseController {
    
    @Autowired
    private ProductService productService;
    
    @GetMapping(path = "/search")
    public @ResponseBody DataTableResults<ProductBean> processSearch(HttpServletRequest req, ProductForm productForm){
        return productService.getDatatable(productForm, req);
    }
    
    @PostMapping(produces = MediaType.APPLICATION_JSON)
    @Transactional
    public @ResponseBody Response saveOrUpdate(HttpServletRequest req, ProductForm form) throws Exception, SysException {
        Long productId = CommonUtil.NVL(form.getProductId());
        ProductBO bo;
        if(productId > 0L) {
            bo = productService.findById(productId);
            if(bo == null) {
                return Response.warning(Constants.RESPONSE_CODE.RECORD_DELETED);
            }
            bo.setUpdatedDate(new Date());
            //bo.setUpdatedBy(jwtService.getUsernameFromRequest(req));
        } else {
            bo = new ProductBO();
            bo.setCreatedDate(new Date());
            //bo.setCreatedBy(jwtService.getUsernameFromRequest(req));
        }
        bo.setBrandId(form.getBrandId());
        bo.setName(form.getName());
        bo.setPrice(form.getPrice());
        bo.setDescription(form.getDescription());
        bo.setCategoryId(form.getCategoryId());
        productService.saveOrUpdate(bo);
        return Response.success(Constants.RESPONSE_CODE.SUCCESS).withData(bo);
    }
}
