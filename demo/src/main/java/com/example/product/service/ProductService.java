package com.example.product.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.common.DataTableResults;
import com.example.common.UttData;
import com.example.product.bean.ProductBean;
import com.example.product.bo.ProductBO;
import com.example.product.dao.ProductDAO;
import com.example.product.form.ProductForm;

@Service
public class ProductService {

    @Autowired
    private ProductDAO productDAO;
    
    @Autowired
    private UttData uttData;
    
    public List<ProductBO> findAll() {
        return productDAO.findAll();
    }
    
    public ProductBO findById(Long productId) {
        return productDAO.findById(productId).orElse(null);
    }
    
    public void saveOrUpdate(ProductBO bo) {
        productDAO.save(bo);
    }
    
    public DataTableResults<ProductBean> getDatatable(ProductForm productForm, HttpServletRequest req) {
        return productDAO.getDatatables(uttData, productForm, req);
    }
}
