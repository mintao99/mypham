package com.example.user.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.user.entity.RoleBO;

@Transactional
@Repository
public interface RoleDAO extends CrudRepository<RoleBO, Long> {

    public List<RoleBO> findAll();
    
    //Lấy ra tat ca role bởi userCode
    @Query("SELECT r.role FROM RoleBO r "
            + " inner join UserRoleBO ur on r.roleId = ur.roleId"
            + " inner join UserBO u on  ur.userId = u.userId "
            + " WHERE LOWER(u.userCode) = LOWER(:userCode)")
    public List<String> findAllRoleOfUserCode(@Param("userCode")String userCode);

}
